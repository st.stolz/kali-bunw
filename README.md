# Kali BUNW

## HowTo

Kali Image builden und starten:

```bash
docker network create kali
docker build -t mykali .
docker run -ti --network kali --name kali -d mykali
docker attach kali
```

## GUI Programm ausführen

1. novnc starten

```bash
docker run -p 8080:8080 --rm --network kali -h novnc -e DISPLAY_WIDTH=1600 -e DISPLAY_HEIGHT=968 -e RUN_XTERM=no theasp/novnc:latest
```

2. in Browser http://localhost:8080/vnc.html aufrufen
3. `docker attach kali` aufrufen und Programm starten
