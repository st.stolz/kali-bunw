FROM kalilinux/kali-rolling

ENV DISPLAY=novnc:0.0

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install kali-linux-headless kali-desktop-core driftnet debconf-utils xinit

WORKDIR /workdir

COPY kali.conf kali.conf
RUN debconf-set-selections < kali.conf